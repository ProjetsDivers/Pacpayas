Pacpayas
---

## Qu'est ce que c'est ?

Juste un foutu Pacman pour la borne d'arcade du fabriquée par leclub Pixel de l'ISIMA.

## Technos & Setup : installation des libs

### SFML

#### Qu'est-ce ?

SFML offre une interface simple vers les différents composants de votre PC, afin de faciliter le développement de jeux ou d'applications multimedia. Elle se compose de cinq modules : système, fenêtrage, graphisme, audio et réseau.

[http://www.sfml-dev.org/index-fr.php](http://www.sfml-dev.org/index-fr.php)

### Installation

```bash
sudo apt-get install libsfml-dev
``` 