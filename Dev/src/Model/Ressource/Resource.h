#ifndef _RESOURCE_H
#define _RESOURCE_H

class Resource
{
public:
    /**
     * @brief Destructor.
     */
    virtual ~Resource() = default;

private:

};

#endif //_RESOURCE_H
