#ifndef _CRYPT_H
#define _CRYPT_H

#include "Model/World/Element.h"

class Crypt
        : public Element
{
public: 
    /**
     * @brief
     * @return
     */
    const Coordinates& getEnterPosition() const;

private: 
    Coordinates m_enterPosition;    ///<
};

#endif //_CRYPT_H
