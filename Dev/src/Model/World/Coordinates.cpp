#include "Coordinates.h"

double Coordinates::getX()
{
    return m_x;
}

double Coordinates::getY()
{
    return m_y;
}

void Coordinates::setX(double x)
{
    m_x = x;
}

void Coordinates::setY(double y)
{
    m_y = y;
}
