#ifndef _TELEPORTER_H
#define _TELEPORTER_H

#include "Model/World/Element.h"

class Teleporter
        : public Element
{
public: 
    /**
     * @brief
     * @return
     */
    const Coordinates& getTarget() const;

private:
    Coordinates m_target;     ///<
};

#endif //_TELEPORTER_H
