#ifndef _ELEMENT_H
#define _ELEMENT_H

#include "Model/World/Coordinates.h"

class Element
{
public: 
    /**
     * @brief Destructor.
     */
    virtual ~Element() = default;

    /**
     * @brief
     * @return
     */
    const Coordinates& getPosition() const;

private:
    Coordinates m_coordinates;      ///<
};

#endif //_ELEMENT_H
