#ifndef _EDGE_H
#define _EDGE_H

class Edge
{
public:
    /**
     * @brief
     * @return
     */
    double getCost() const;

private:
    double m_cost;  ///<
};

#endif //_EDGE_H
