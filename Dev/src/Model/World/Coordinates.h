#ifndef _COORDINATES_H
#define _COORDINATES_H

class Coordinates
{
public: 
    /**
     * @brief
     * @return
     */
	double getX();
	
    /**
     * @brief
     * @return
     */
	double getY();
	
    /**
     * @brief
     * @param x
     */
	void setX(double x);
	
    /**
     * @brief
     * @param y
     */
	void setY(double y);

private: 
    double m_x;     ///<
    double m_y;     ///<
};

#endif //_COORDINATES_H
