#ifndef _GAME_H
#define _GAME_H

class Game
{
public:
    /**
     * @brief
     * @return
     */
    unsigned int getLevel() const;
	
    /**
     * @brief
     */
	void nextLevel();
	
    /**
     * @brief
     * @return
     */
    unsigned int getNbLifes() const;

private: 
    unsigned int m_level;       ///<
    unsigned int m_pacmanLifes; ///<
};

#endif //_GAME_H
