#include "Score.h"

const std::string& Score::getName() const
{
    return m_name;
}

void Score::setName(const std::string& name)
{
    m_name = name;
}

unsigned int Score::getScore() const
{
    return m_score;
}

void Score::setScore(unsigned int score)
{
    m_score = score;
}
