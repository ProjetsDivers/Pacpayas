#ifndef _SCORE_H
#define _SCORE_H

#include <string>

class Score
{
public: 
    /**
     * @brief
     * @return
     */
    const std::string& getName() const;
	
    /**
     * @brief
     * @param name
     */
    void setName(const std::string& name);
	
    /**
     * @brief
     * @return
     */
    unsigned int getScore() const;
	
	/**
	 * @param score
	 */
	void setScore(unsigned int score);

private: 
    std::string m_name;     ///<
    unsigned int m_score;   ///<
};

#endif //_SCORE_H
