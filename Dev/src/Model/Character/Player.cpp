#include "Player.h"

void Player::move()
{

}

Direction Player::getWantedDirection() const
{
    return m_wantedDirection;
}

void Player::setWantedDirection(Direction direction)
{
    m_wantedDirection = direction;
}

unsigned int Player::getPlayerId() const
{
    return m_playerId;
}
