#ifndef _GHOSTSTATE_H
#define _GHOSTSTATE_H

/**
 * @brief The GhostState enum Store a ghost state.
 */
enum GhostState
{
    HUNTING,
    ESCAPING,
    BACK_TO_CRYPT
};

#endif //_GHOSTSTATE_H
