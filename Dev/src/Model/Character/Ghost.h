#ifndef _GHOST_H
#define _GHOST_H

#include "Model/Character/Character.h"
#include "Model/Character/GhostState.h"

class Ghost
        : public Character
{
public: 
    /**
     * @brief
     */
	void move();
	
    /**
     * @brief
     * @return
     */
    GhostState getState() const;

    /**
     * @brief
     * @param state
     */
	void setState(GhostState state);
	
    /**
     * @brief
     * @return
     */
    //Path getPath();

protected: 
    /**
     * @brief
     */
	void computePath();

private: 
    GhostState m_state;     ///<
};

#endif //_GHOST_H
