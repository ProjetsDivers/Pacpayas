#ifndef _DIRECTION_H
#define _DIRECTION_H

/**
 * @brief The Direction enum Store a direction.
 */
enum Direction
{
    UP,
    DOWN,
    RIGHT,
    LEFT,
    NONE
};

#endif //_DIRECTION_H
