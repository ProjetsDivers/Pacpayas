#ifndef _PLAYER_H
#define _PLAYER_H

#include "Character.h"

class Player
        : public Character
{
public: 
    /**
     * @brief
     */
	void move();
	
    /**
     * @brief
     * @return
     */
    Direction getWantedDirection() const;
	
    /**
     * @brief
     * @param direction
     */
	void setWantedDirection(Direction direction);
	
    /**
     * @brief
     * @return
     */
    unsigned int getPlayerId() const;

private: 
    static unsigned int m_playerCount;  ///<

    unsigned int m_playerId;            ///<
    Direction m_wantedDirection;        ///<
};

#endif //_PLAYER_H
