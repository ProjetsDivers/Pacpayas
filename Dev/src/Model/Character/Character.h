#ifndef _CHARACTER_H
#define _CHARACTER_H

#include "Model/Character/Direction.h"
#include "Model/World/Element.h"

class Character
        : public Element
{
public: 
	
    /**
     * @brief
     */
    virtual void move() =0;
	
    /**
     * @brief
     * @return
     */
    Direction getDirection() const;
	
    /**
     * @brief
     * @param direction
     */
	void setDirection(Direction direction);

protected: 
    double m_speed;         ///<

private: 
    Direction m_direction;  ///<
};

#endif //_CHARACTER_H
