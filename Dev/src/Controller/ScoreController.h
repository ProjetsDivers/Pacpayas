#ifndef _SCORECONTROLLER_H
#define _SCORECONTROLLER_H

class ScoreController
{
public: 
    /**
     * @brief
     */
	void saveScore();
	
    /**
     * @brief
     */
	void loadScore();
};

#endif //_SCORECONTROLLER_H
