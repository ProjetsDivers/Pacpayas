#ifndef _SOUNDCONTROLLER_H
#define _SOUNDCONTROLLER_H

class SoundController
{
public: 
	
    /**
     * @brief
     * @return
     */
    bool isMuted() const;
	
    /**
     * @brief
     * @param state
     */
	void setMuted(bool state);

private: 
    bool m_muted;       ///<
};

#endif //_SOUNDCONTROLLER_H
