#include "SoundController.h"

bool SoundController::isMuted() const
{
    return m_muted;
}

void SoundController::setMuted(bool state)
{
    m_muted = state;
}
